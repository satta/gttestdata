#!/usr/bin/perl -w

use strict;
use Bio::Tools::GFF;

my $gff3file = $ARGV[0];

# create GFF3 parser
my $gffio = Bio::Tools::GFF->new(-file=>"$gff3file", -gff_version=>3);

my @features;

while( my $feature = $gffio->next_feature() ) {
  push @features, $feature # store feature
}
