#!/bin/sh -e

VALGRIND="valgrind --tool=massif --alloc-fn=gt_malloc --alloc-fn=gt_calloc --alloc-fn=gt_realloc --alloc-fn=xmalloc --alloc-fn=xcalloc --alloc-fn=xrealloc"

for FILE in tair.gff3 fruitfly.gff3 ensembl.gff3
do
  for BIN in gt-32bit gt-64bit
  do
    echo $FILE $BIN
    time $VALGRIND --massif-out-file="${BIN}_$FILE.out" ./$BIN gff3 -show no $FILE
    echo

    echo $FILE $BIN
    time $VALGRIND --massif-out-file="${BIN}_-sort_$FILE.out" ./$BIN gff3 -show no -sort $FILE
    echo
  done
done
